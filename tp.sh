#!/bin/bash

hola () {
  echo Hola mundo!
}

chau () {
  echo Chau mundo!
}

suma () {
  echo $(($1 + $2))
}

multiplicar () {
  echo $(($1 * $2))
}

resta () {
  echo $(($1 - $2))
}

dividir () {
  echo $(($1 / $2))
}

hola
chau
suma 3 4
multiplicar 3 4
resta 3 2
dividir 10 2
